<?php
/*
  ./noyau/constantes.php
  Constantes du framework
*/

//Pour que chaque lien débute par la même base

$local_path = str_replace(basename($_SERVER['SCRIPT_NAME']) , '', $_SERVER['SCRIPT_NAME']);

define('ROOT_ADMIN', 'http://'
              . $_SERVER['HTTP_HOST']
              . $local_path);

define('ROOT_PUBLIC', 'http://'
              . $_SERVER['HTTP_HOST']
              . str_replace(ADMIN_FOLDER, PUBLIC_FOLDER, $local_path));
