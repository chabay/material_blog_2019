<?php
/*
    ./app/controleurs/categoriesControleur.php
*/
namespace App\Controleurs\CategoriesControleur;
use \App\Modeles\CategoriesModele AS Categorie;

// LISTE DES CATEGORIES

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);

  GLOBAL $content1, $title;
  $title = "Liste des catégories";
  ob_start();
  include '../app/vues/categories/index.php';
  $content1 = ob_get_clean();
}

// AJOUTER UNE CATEGORIE
// Formulaire d'ajout
function addFormAction() {
  GLOBAL $title, $content1;
  $title = "Ajout d'une catégorie";
  ob_start();
  include '../app/vues/categories/addForm.php';
  $content1 = ob_get_clean();
}
// Ajout dans la base de données et retour à la liste
function insertAction(\PDO $connexion, string $titre) {
  include '../app/modeles/categoriesModele.php';
  Categorie\addOne($connexion, $titre);
  header('location:' . ROOT_ADMIN . 'categories');
}

//SUPPRIMER UNE CATEGORIE

function deleteAction(\PDO $connexion, int $id) {
  include '../app/modeles/categoriesModele.php';
  //Mettre le deleteOneById dans un $return =
  //si on veut renvoyer un message d'erreur
  //return du execute dans le Modèle en le mettant dans un intVal()
  Categorie\deleteOneById($connexion, $id);
  header('location:' . ROOT_ADMIN . 'categories');
}


//EDITER UNE CATEGORIE
 function editFormAction(\PDO $connexion, int $id) {
   include '../app/modeles/categoriesModele.php';
   $categorie = Categorie\findOneById($connexion, $id);
   GLOBAL $title, $content1;
   $title = "Editer la catégorie";
   ob_start();
   include '../app/vues/categories/editForm.php';
   $content1 = ob_get_clean();
 }

function updateAction(\PDO $connexion, int $id, string $titre) {
  include '../app/modeles/categoriesModele.php';
  Categorie\updateOneById($connexion, $id, $titre);
  header('location:' . ROOT_ADMIN . 'categories');
}


// DETAIL D'UNE CATEGORIE

function showAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/categoriesModele.php';
  $categorie = Categorie\findOneById($connexion, $id);

  include_once '../app/modeles/postsModele.php';
  $posts = \App\Modeles\PostsModele\findAllByCategorie($connexion, $id);

  GLOBAL $title, $content1;

  $title = $categorie['titre'];

  ob_start();
  include '../app/vues/categories/show.php';
  $content1 = ob_get_clean();

}
