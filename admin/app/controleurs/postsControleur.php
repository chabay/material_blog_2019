<?php
/*
    ./app/controleurs/postsControleur.php
*/
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;


// LISTE DES ARTICLES

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);

  GLOBAL $title, $content1;

  $title = POST_INDEX_TITLE;

  ob_start();
  include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();

}

// AJOUT D'UN ARTICLE
// Formulaire d'ajout
function addFormAction(\PDO $connexion) {
  //Je vais chercher la liste des auteurs
  include '../app/modeles/auteursModele.php';
  $auteurs = \App\Modeles\AuteursModele\findall($connexion);

  //Je vais chercher la liste des categories
  include '../app/modeles/categoriesModele.php';
  $categories = \App\Modeles\CategoriesModele\findall($connexion);

  //Je charge la vue addForm dans $content1
  GLOBAL $title, $content1;

  $title = "Ajout d'un article";

  ob_start();
  include '../app/vues/posts/addForm.php';
  $content1 = ob_get_clean();
}

// Ajout dans la base de données

function insertAction(\PDO $connexion, array $data) {
  //Je demande au modèle d'ajouter le post
  include '../app/modeles/postsModele.php';
  $id = Post\addOne($connexion, $data);
  //Je demande au modèle d'ajouter la catégorie correspondante
  foreach ($_POST['categories'] as $categorieID):
    Post\addCategorieById($connexion, $id, $categorieID);
  endforeach;
  //Je redirige vers la liste des posts
  header('location:' . ROOT_ADMIN . 'posts');
}

//SUPRESSION D'UN ARTICLE
function deleteAction(\PDO $connexion, int $id) {
  //Je demande au modèle de supprimer les liaisons n-m correspondantes
  include '../app/modeles/postsModele.php';
  Post\deletePostHasCategoriesByPostId($connexion, $id);
  //Je demande au modèle de supprimer le post
  include_once '../app/modeles/postsModele.php';
  Post\deleteOneById($connexion, $id);
  header('location:' . ROOT_ADMIN . 'posts');
}

//EDITION D'UN ARTICLE
//FORMULAIRE
function editFormAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/postsModele.php';
  $post = Post\findOneById($connexion, $id);

  //Je demande les catégories du post
  include_once '../app/modeles/postsModele.php';
  $postCategories = Post\findCategoriesByPostId($connexion, $id);

  //Je vais chercher la liste des auteurs
  include '../app/modeles/auteursModele.php';
  $auteurs = \App\Modeles\AuteursModele\findall($connexion);

  //Je vais chercher la liste des catégories
  include '../app/modeles/categoriesModele.php';
  $categories = \App\Modeles\CategoriesModele\findall($connexion);

  GLOBAL $title, $content1;
  $title = "Editer le post";
  ob_start();
  include '../app/vues/posts/editForm.php';
  $content1 = ob_get_clean();

}

//UPDATE
function updateAction(\PDO $connexion, int $id) {
  //Je demande au modèle de supprimer les catégories correspondantes
  include '../app/modeles/postsModele.php';
  Post\deletePostHasCategoriesByPostId($connexion, $id);
  //Je demande au modèle de modifier le post
  Post\updateOneById($connexion, $id, $_POST);
  //Je demande au modèle d'ajouter les catégories correspondantes
  foreach ($_POST['categories'] as $categorieID):
    Post\addCategorieById($connexion, $id, $categorieID);
  endforeach;
  //Je redirige vers la liste des posts
  header('location:' . ROOT_ADMIN . 'posts');
}




// DETAIL D'UN ARTICLE

function showAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/postsModele.php';
  $post = Post\findOneById($connexion, $id);

  GLOBAL $title, $content1;

  $title = $post['titre'];

  ob_start();
  include '../app/vues/posts/show.php';
  $content1 = ob_get_clean();

}
