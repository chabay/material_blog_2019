<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\UsersControleur;
use \App\Modeles\UsersModele AS User;

function dashboardAction() {

  GLOBAL $title, $content1;
  $title = USER_DASHBOARD_TITLE;
  ob_start();
  include '../app/vues/users/dashboard.php';
  $content1 = ob_get_clean();
}

function logoutAction() {
  //session_destroy();
  unset($_SESSION['user']);
  header('location: ' . ROOT_PUBLIC . 'login');
}
