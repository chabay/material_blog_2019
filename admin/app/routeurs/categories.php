<?php
/*
  ./app/routeurs/categories.php
  Routes des categories
  Il existe un $_GET['categorie']
*/
use \App\Controleurs\CategoriesControleur;

include_once '../app/controleurs/categoriesControleur.php';

switch ($_GET['categories']):
  /*
      Listes des catégories
      PATTERN: /?categories=index
      CTRL: categoriesControleur
      ACTION: indexAction
   */
 case 'index':
   CategoriesControleur\indexAction($connexion);
 break;
 case 'addform':
   CategoriesControleur\addFormAction();
 break;
 case 'insert':
   CategoriesControleur\insertAction($connexion, $_POST['titre']);
 break;
 case 'delete':
   CategoriesControleur\deleteAction($connexion, $_GET['id']);
 break;
 case 'edit':
   CategoriesControleur\editFormAction($connexion, $_GET['id']);
 break;
 case 'update':
   CategoriesControleur\updateAction($connexion, $_GET['id'], $_POST['titre']);
 break;
endswitch;
