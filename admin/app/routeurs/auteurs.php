<?php
/*
  ./app/routeurs/auteurs.php
  Routes des auteurs
  Il existe un $_GET['auteurs']
*/
  use \App\Controleurs\AuteursControleur;

  include_once '../app/controleurs/auteursControleur.php';



switch ($_GET['auteurs']):
  case 'index':
    /*
       Liste des auteurs
       PATTERN: /?auteurs=index
       CTRL: auteursControleur
       ACTION: index
    */
    AuteursControleur\indexAction($connexion);
  break;

endswitch;
