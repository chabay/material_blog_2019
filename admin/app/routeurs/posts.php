<?php
/*
  ./app/routeurs/posts.php
  Routes des posts
  Il existe un $_GET['post']
*/
  use \App\Controleurs\PostsControleur;

  include_once '../app/controleurs/postsControleur.php';



switch ($_GET['posts']):
  case 'index':
    /*
       Liste des articles
       PATTERN: /?posts=index
       CTRL: postsControleur
       ACTION: index
       !!! ON PEUT METTRE CA DANS LE DEFAULT SANS METTRE LA VARIABLE D'URL INDEX
    */
    PostsControleur\indexAction($connexion);
  break;
  case 'addform':
    PostsControleur\addFormAction($connexion);
  break;
  case 'insert':
    PostsControleur\insertAction($connexion, [
      'titre' => $_POST['titre'],
      'slug' => $_POST['slug'],
      'texte' => $_POST['texte'],
      'auteur' => $_POST['auteur']
    ]);
  break;
  case 'delete':
    PostsControleur\deleteAction($connexion, $_GET['id']);
  break;
  case 'edit':
    PostsControleur\editFormAction($connexion, $_GET['id']);
  break;
  case 'update':
    PostsControleur\updateAction($connexion, $_GET['id']);
  break;

endswitch;
