<?php
  /*
    ./app/vues/posts/search.php
  Variables disponibles :
  - $search QuickHashIntStringHash
  - $posts ARRAY(ARRAY(id, titre, slug, datePublication, media, texte, auteur, categorie))
  */
?>


<h1 class="page-header">
  Résultats de la recherche :
    <small><?php echo $search; ?></small>
</h1>


<?php
foreach ($posts as $post): ?>
<!-- Article -->
<article>
  <h2>
      <a href="post/<?php echo $post['postId']; ?>/<?php echo $post['postSlug']; ?>">
      <?php echo $post['postTitre']; ?></a>
  </h2>
  <p class="lead">
    by <a href="#"><?php echo $post['pseudo']; ?></a>
  </p>
  <p> Posted on
    <?php
      echo \Noyau\Fonctions\formater_date($post['datePublication']);
 ?>     </p>
  <hr>
  <img class="img-responsive z-depth-2" src="<?php //echo $post['media']; ?>" alt="">
  <hr>
     <div><?php
     echo \Noyau\Fonctions\tronquer($post['texte']);
     ?></div>
  <a href="post/<?php echo $post['postId']; ?>/<?php echo $post['postSlug']; ?>">
    <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
  </a>
  <hr>
</article>
<!-- Fin article -->
<?php endforeach; ?>
