<?php
  /*
    ./app/vues/auteurs/index.php
  Variables disponibles :
  - $auteurs ARRAY(ARRAY(id, pseudo, mdp))
  */
?>


<h1>Gestion des auteurs</h1>
<div><a href="auteurs/add">Ajouter un enregistrement</a></div>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>Id</th>
      <th>Pseudo</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($auteurs as $auteur): ?>
        <tr>
      <td><?php echo $auteur['id']; ?></td>
      <td><?php echo $auteur['pseudo']; ?></td>
      <td>
        <a href="#">Edit</a> |
        <a href="posts/delete/2">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
    </tbody>
</table>
