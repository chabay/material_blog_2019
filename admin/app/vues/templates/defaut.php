<?php
/*
  ./app/vues/templates/defaut.php
 */
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include '../app/vues/templates/partials/_head.php'; ?>
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
  <?php include '../app/vues/templates/partials/_nav.php'; ?>
</nav>

    <div class="container theme-showcase" role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">

        <?php echo $content1; ?>
      </div>
      <!-- /container -->

      </div>

    <!-- Scripts -->
    <?php include '../app/vues/templates/partials/_scripts.php'; ?>
  </body>
</html>
