<?php
  /*
    ./app/vues/categories/editForm.php
  Variables disponibles :
  - $categories ARRAY(ARRAY(id, titre, slug))
  */
?>

<h1>Modification de la catégorie '<?php echo $categorie['titre']; ?>'</h1>
<div>
  <a href="categories">
    Retour vers la liste des catégories
  </a>
</div>

<form action="categories/<?php echo $categorie['id']; ?>/edit/update" method="post">
  <div>
    <label for="titre">Titre</label>
    <input type="text" name="titre" id="titre" value="<?php echo $categorie['titre']; ?>" />
  </div>


  <div><input type="submit" /></div>
</form>
