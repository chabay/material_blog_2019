<?php
  /*
    ./app/vues/categories/index.php
  Variables disponibles :
  - $categories ARRAY(ARRAY(id, titre, slug))
  */
?>


<h1>Gestion des catégories</h1>
<div><a href="categories/add/form">Ajouter un enregistrement</a></div>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>Id</th>
      <th>Titre</th>
      <th>Slug</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($categories as $categorie): ?>
        <tr>
      <td><?php echo $categorie['id']; ?></td>
      <td><?php echo $categorie['titre']; ?></td>
      <td><?php echo $categorie['slug']; ?></td>
      <td>
        <a href="categories/<?php echo $categorie['id']; ?>/edit/form">Edit</a> |
        <a href="categories/<?php echo $categorie['id']; ?>/delete">Delete</a>
      </td>
    </tr>
  <?php endforeach; ?>
    </tbody>
</table>
