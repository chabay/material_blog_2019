<?php
/*
  ./app/routeur.php
 */


if (isset($_GET['users'])):
    include_once '../app/routeurs/users.php';

elseif (isset($_GET['posts'])):
   include_once '../app/routeurs/posts.php';

elseif (isset($_GET['categories'])):
  include_once '../app/routeurs/categories.php';

elseif (isset($_GET['auteurs'])):
  include_once '../app/routeurs/auteurs.php';

  /*
    ROUTE PAR DEFAUT:
   	PATTERN: /
   	CTRL: usersControleur
   	ACTION: dashboard
  */

 else: include_once '../app/controleurs/usersControleur.php';
 \App\Controleurs\UsersControleur\dashboardAction($connexion);
endif;
