<?php
  /*
    ./app/modeles/categoriesModele.php
  */

  namespace App\Modeles\CategoriesModele;

// LISTE DES CATEGORIES

function findAll(\PDO $connexion) {
  $sql = 'SELECT *
          FROM categories
          ORDER BY titre ASC;';
  $rs = $connexion->query($sql);

  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}


// AJOUT D'UNE CATEGORIE

function addOne(\PDO $connexion, string $titre) {
  $sql = 'INSERT INTO categories
          SET titre = :titre,
              slug = :slug;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':titre', $titre, \PDO::PARAM_STR);
  $rs->bindValue(':slug', \Noyau\Fonctions\slugify($titre), \PDO::PARAM_STR);
  $rs->execute();
}


// SUPPRESSION D'UNE CATEGORIE

function deleteOneById(\PDO $connexion, int $id) {
  $sql = 'DELETE FROM categories
          WHERE id = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
}



// EDITION D'UNE CATEGORIE
// Formulaire d'édition
function findOneById(\PDO $connexion, int $id) {
  $sql = 'SELECT *
          FROM categories
          WHERE id = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();

  return $rs->fetch(\PDO::FETCH_ASSOC);
}
// Validation du formulaire d'édition
function updateOneById(\PDO $connexion, int $id, string $titre) {
  $sql = 'UPDATE categories
          SET titre = :titre,
              slug = :slug
          WHERE id = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->bindValue(':titre', $titre, \PDO::PARAM_STR);
  $rs->bindValue(':slug', \Noyau\Fonctions\slugify($titre), \PDO::PARAM_STR);
  $rs->execute();
}
