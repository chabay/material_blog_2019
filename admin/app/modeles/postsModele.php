<?php
  /*
    ./app/modeles/postsModele.php
  */

  namespace App\Modeles\PostsModele;

// LISTE DES ARTICLES

  function findAll(\PDO $connexion) {
    $sql = 'SELECT *, posts.id AS postId
            FROM posts
            JOIN auteurs ON posts.auteur = auteurs.id
            ORDER BY posts.id DESC;';
    $rs = $connexion->query($sql);

    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

// DETAIL D'UN ARTICLE

  function findOneById(\PDO $connexion, int $id) {
    $sql = 'SELECT *, posts.id AS postId
            FROM posts
            JOIN auteurs ON posts.auteur = auteurs.id
            WHERE posts.id = :id;';
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();

    return $rs->fetch(\PDO::FETCH_ASSOC);
  }


//AJOUT D'UN POST
function addOne(\PDO $connexion, array $data){
  $sql = "INSERT INTO posts
          SET titre = :titre,
              slug = :slug,
              texte = :texte,
              auteur = :auteur,
              datePublication = NOW();";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindValue(':slug', $data['slug'], \PDO::PARAM_STR);
  $rs->bindValue(':texte', $data['texte'], \PDO::PARAM_STR);
  $rs->bindValue(':auteur', $data['auteur'], \PDO::PARAM_STR);
  $rs->execute();
  return $connexion->lastInsertId();
}
//+ AJOUT DES CATEGORIES DU POST
function addCategorieById(\PDO $connexion, int $postID, int $categorieID){
  $sql = "INSERT INTO posts_has_categories
          SET post = :post,
              categorie = :categorie;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $postID, \PDO::PARAM_INT);
  $rs->bindValue(':categorie', $categorieID, \PDO::PARAM_INT);
  $rs->execute();

}

//SUPPRESSION D'UN POST
function deletePostHasCategoriesByPostId(\PDO $connexion, int $id){
  $sql = "DELETE FROM posts_has_categories
          WHERE post = :post;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $id, \PDO::PARAM_INT);
  $rs->execute();
}
function deleteOnebyId(\PDO $connexion, int $id){
  $sql = "DELETE FROM posts
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
}

//EDITION D'UN POST
//Affichage des catégories sélectionnées dans le formulaire
function findCategoriesByPostId(\PDO $connexion, int $id){
  $sql = "SELECT categorie
          FROM posts_has_categories
          WHERE post = :post;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetchAll(\PDO::FETCH_COLUMN);
}

//Update dans la db
function updateOneById(\PDO $connexion, int $id, array $data){
  $sql = "UPDATE posts
          SET titre = :titre,
              slug = :slug,
              texte = :texte,
              auteur = :auteur
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindValue(':slug', $data['slug'], \PDO::PARAM_STR);
  $rs->bindValue(':texte', $data['texte'], \PDO::PARAM_STR);
  $rs->bindValue(':auteur', $data['auteur'], \PDO::PARAM_INT);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
}
