<?php
/*
  ./app/routeurs/categories.php
  Routes des categories
  Il existe un $_GET['categorie']
*/
use \App\Controleurs\CategoriesControleur;

include_once '../app/controleurs/categoriesControleur.php';

switch ($_GET['categorie']):
  /*
      ROUTE DU DETAIL D'UNE CATEGORIE
      PATTERN: /?categorie=show&id=x
      CTRL: categoriesControleur
      ACTION: showAction
   */
  case 'show':
 CategoriesControleur\showAction($connexion, $_GET['id']);
 break;
endswitch;
