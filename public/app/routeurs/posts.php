<?php
/*
  ./app/routeurs/posts.php
  Routes des posts
  Il existe un $_GET['post']
*/
  use \App\Controleurs\PostsControleur;

  include_once '../app/controleurs/postsControleur.php';



switch ($_GET['post']):
  case 'show':
    /*
       ROUTE DU DETAIL D'UN ARTICLE
       PATTERN: /?post=show&id=x
       CTRL: postsControleur
       ACTION: showAction
    */
    PostsControleur\showAction($connexion, $_GET['id']);
  break;
  case 'search':
    /*
       RECHERCHE D'UN POST
       PATTERN: /?post=search
       CTRL: postsControleur
       ACTION: search
    */
    PostsControleur\searchAction($connexion, $_POST['search']);
  break;
endswitch;
