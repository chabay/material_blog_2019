<?php /*
../app/vues/templates/partials/_nav.php
*/ ?>


<!-- Navigation -->
<nav class="navbar z-depth-2 info-color">
<div class="container ">
  <div class="navbar-header">

    <a class="navbar-brand waves-effect waves-light" href="">
      Mon Super Blog
    </a>
  </div>

  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

    <form action="post/search" method="post" class="navbar-form navbar-right waves-effect waves-light" role="search">
      <div class="form-group">
        <input type="text" name="search" class="form-control" placeholder="Search">
      </div>
    </form>
  </div>
</div>
</nav>
