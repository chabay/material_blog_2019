<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\UsersControleur;
use \App\Modeles\UsersModele AS User;

function loginFormAction() {
  //Rien à aller chercher dans le modèle
  //Je charge la vue loginForm dans $content1
  GLOBAL $content1;
  ob_start();
  include '../app/vues/users/loginForm.php';
  $content1 = ob_get_clean();
}

function loginVerificationAction(\PDO $connexion, array $data) {
  //Je vais chercher le user qui correspond au login et au mot de passe
  include_once '../app/modeles/usersModele.php';
  $user = User\findOneByLoginAndPassword($connexion, $data);


  //Si j'ai trouvé un user, on le redirige vers le backoffice
  //Sinon, je le redirige vers le formulaire avec un message d'erreur
  if ($user):
    //Pour protéger l'accès à /admin, on crée une variable de session
    //Ne pas oublier de lancer la session dans init
    //Faire la même chose dans /admin
    $_SESSION['user'] = $user;
    header('location: ' . ROOT_ADMIN);
  else:
    header('location: ' . ROOT_PUBLIC . 'login?error=1');
  endif;

}
