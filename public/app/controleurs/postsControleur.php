<?php
/*
    ./app/controleurs/postsControleur.php
*/
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;


// LISTE DES ARTICLES

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);

  GLOBAL $title, $content1;

  $title = POST_INDEX_TITLE;

  ob_start();
  include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();

}

// DETAIL D'UN ARTICLE

function showAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/postsModele.php';
  $post = Post\findOneById($connexion, $id);

  GLOBAL $title, $content1;

  $title = $post['titre'];

  ob_start();
  include '../app/vues/posts/show.php';
  $content1 = ob_get_clean();

}


// RECHERCHE D'UN ARTICLE

function searchAction(\PDO $connexion, string $search) {
  //Je demande la liste des posts au modèle
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAllBySearch($connexion, $search);

  //Je charge la vue search dans $content1
  GLOBAL $title, $content1;

  $title = $search;

  ob_start();
  include '../app/vues/posts/search.php';
  $content1 = ob_get_clean();


}
