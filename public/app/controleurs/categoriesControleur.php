<?php
/*
    ./app/controleurs/categoriesControleur.php
*/
namespace App\Controleurs\CategoriesControleur;
use \App\Modeles\CategoriesModele AS Categorie;

// LISTE DES CATEGORIES

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);

  include '../app/vues/categories/index.php';

}


// DETAIL D'UNE CATEGORIE

function showAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/categoriesModele.php';
  $categorie = Categorie\findOneById($connexion, $id);

  include_once '../app/modeles/postsModele.php';
  $posts = \App\Modeles\PostsModele\findAllByCategorie($connexion, $id);

  GLOBAL $title, $content1;

  $title = $categorie['titre'];

  ob_start();
  include '../app/vues/categories/show.php';
  $content1 = ob_get_clean();

}
