<?php
/*
  ./app/routeur.php
 */

 /*
     CHARGEMENT DU ROUTEUR DES POSTS
     PATTERN: index.php?post=x
 */

if (isset($_GET['post'])):
  include_once '../app/routeurs/posts.php';

 /*
    CHARGEMENT DU ROUTEUR DES CATEGORIES
    PATTERN: index.php?categorie=x
 */

elseif (isset($_GET['categorie'])):
  include_once '../app/routeurs/categories.php';

  /*
    CHARGEMENT DU ROUTEUR DES USERS
    PATTERN : index.php?users=x
  */

 elseif (isset($_GET['users'])):
   include_once '../app/routeurs/users.php';

  /*
    ROUTE PAR DEFAUT: liste des articles
   	PATTERN: /
   	CTRL: postsControleur
   	ACTION: indexAction
  */

else: include_once '../app/controleurs/postsControleur.php';
 \App\Controleurs\PostsControleur\indexAction($connexion);

endif;
