<?php
  /*
    ./app/modeles/postsModele.php
  */

  namespace App\Modeles\PostsModele;

// LISTE DES ARTICLES

  function findAll(\PDO $connexion) {
    $sql = 'SELECT *, posts.id AS postId
            FROM posts
            JOIN auteurs ON posts.auteur = auteurs.id
            ORDER BY datePublication DESC
            LIMIT 5;';
    $rs = $connexion->query($sql);

    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

// DETAIL D'UN ARTICLE

  function findOneById(\PDO $connexion, int $id) {
    $sql = 'SELECT *
            FROM posts
            JOIN auteurs ON posts.auteur = auteurs.id
            WHERE posts.id = :id;';
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();

    return $rs->fetch(\PDO::FETCH_ASSOC);
  }

// LISTE DES ARTICLES PAR CATEGORIE

function findAllByCategorie(\PDO $connexion, int $id) {
  $sql = 'SELECT *, posts.id AS postId
          FROM posts
          JOIN posts_has_categories ON post = posts.id
          JOIN auteurs ON posts.auteur = auteurs.id
          WHERE categorie = :id
          ORDER BY datePublication DESC;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();

  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}


// RECHERCHE D'UN ARTICLE

function findAllBySearch(\PDO $connexion, string $search) {
  $words = explode(' ', trim($search));
  $sql = "SELECT DISTINCT posts.id AS postId,
                          posts.titre AS postTitre,
                          posts.slug AS postSlug,
                          media, texte, datePublication, pseudo
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          JOIN posts_has_categories ON post = posts.id
          JOIN categories ON categorie = categories.id
          WHERE 1 = 0 ";
  for ($i=0; $i<count($words); $i++):
    $sql .= "OR posts.titre LIKE :word$i
             OR texte LIKE :word$i
             OR categories.titre LIKE :word$i
             OR pseudo LIKE :word$i ";
  endfor;
  $sql .= "ORDER BY datePublication DESC;";

  $rs = $connexion->prepare($sql);
  for ($i=0; $i<count($words); $i++):
    $rs->bindValue(":word$i", '%' . $words[$i] . '%', \PDO::PARAM_STR);
  endfor;
  $rs->execute();

  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
