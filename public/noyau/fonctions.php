<?php
/*./noyau/fonctions.php*/

namespace Noyau\Fonctions;

/* TRAITEMENT DES CHAINES DE CARACTERES */
/**
 * tronquer
 * @param  string  $chaine
 * @param  integer $nbreCaracteres [valeur par défaut]
 * @return string
 */

function tronquer(string $chaine, int $nbreCaracteres = 200) :string {
    if(strlen($chaine) > $nbreCaracteres): //Si jamais la chaîne est plus petite que 200 caractères.
    $positionEspace = strpos($chaine, ' ', $nbreCaracteres);
    return substr($chaine, 0, $positionEspace);
  else:
    return $chaine;
  endif;

}

/* TRAITEMENT DES DATES */
/**
 * formater une date avec un format par défaut
 * @param  string $date
 * @param  string $format [format par défaut]
 * @return string         
 */

function formater_date(string $date, string $format = 'D d M Y') :string {
  return date_format(date_create($date), $format);
}
